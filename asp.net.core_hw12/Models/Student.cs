﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.net.core_hw12.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }      
    }
}
