﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.net.core_hw12.Models
{
    public class StudentContext
    { 
        public IList<Student> Students { get; set; } = new List<Student>();
    }
}
