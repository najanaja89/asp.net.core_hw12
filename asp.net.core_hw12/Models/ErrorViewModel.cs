using System;
using NLog;

namespace asp.net.core_hw12.Models
{
    public class ErrorViewModel
    {
        private readonly ILogger _logger;
        public ErrorViewModel(ILogger logger)
        {
            _logger = logger;
            _logger.Error("error Page");
        }

        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
