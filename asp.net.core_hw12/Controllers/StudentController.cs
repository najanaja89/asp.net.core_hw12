﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using asp.net.core_hw12.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace asp.net.core_hw12.Controllers
{
    public class StudentController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private static List<Student> _students = new List<Student>()
        {
            new Student() { Id = 1, Name = "Bruce Wayne"},
            new Student() { Id = 2, Name = "Barry Allen"},
            new Student() { Id = 3, Name = "John Jhonzz"},
            new Student() { Id = 4, Name = "Diana Prince"},
            new Student() { Id = 5, Name = "Oliver Queen"},
        };

        public StudentController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        // GET: Student
        public ActionResult Index()
        {
            return View(_students);
        }

        // GET: Student/Details/5
        public ActionResult Details(int id)
        {
            _logger.LogWarning("Redirect to Details");
            var student = _students.FirstOrDefault(x => x.Id == id);
            return View(student);
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Student student)
        {
            try
            {
                // TODO: Add insert logic here
                int idMax = _students.Max(x => x.Id);
                student.Id = ++idMax;
                _students.Add(student);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int id)
        {
            var student = _students.FirstOrDefault(x => x.Id == id);
            return View(student);
        }

        // POST: Student/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Student student)
        {
            try
            {
                // TODO: Add update logic here
                int index = 0;
                foreach (var item in _students)
                {
                    if (item.Id == student.Id)
                    {
                        index = _students.IndexOf(item);
                    }
                }

                _students[index] = student;
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            var student = _students.FirstOrDefault(x => x.Id == id);
            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Student student)
        {
            try
            {
                int index = 0;
                foreach (var item in _students)
                {
                    if (item.Id == student.Id)
                    {
                        index = _students.IndexOf(item);
                    }
                }
                _students.RemoveAt(index);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}